import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/home/Home.vue';
import Secondary from './views/Secondary.vue';
import Login from './views/Login.vue';
import Signup from './views/Signup.vue';
import ScheduleCall from './views/ScheduleCall.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/secondary',
      name: 'secondary',
      component: Secondary
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup
    },
    {
      path: '/schedule-call',
      name: 'schedule-call',
      component: ScheduleCall
    },
    
  ],
  mode: 'history'
});
